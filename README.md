## Model Database

The csv files in the model folder contain all of the meta data about the current batch of models. The uuid field is the
name for the file that contains the model data for reloading. Note: You may see many models but no data files; this is
because it was too space-intensive for us to upload the model data to github. However, you can regenerate the data by
training the relevant estimator using the parameters given in the csv file.

## Dependencies
We have conveniently included all project dependencies in pymodules.txt. You can install them with
``pip install -r pymodules.txt``.


## Running the Codebase

Everything is driven from pipeline.py within python/pipeline. You can see the three stages we used to search the
hyper parameter space and predict the Kaggle data. Data is loaded using the python/Data.py class, it is put through
the preprocessing pipeline (with transformers in python/preprocessors), and then one of the estimators can be trained on
it. Our two estimators (DNN and DSAE) can be found in python/estimators (they both inherit DBInterface.py which is what
we use for loading and saving model metadata). All estimators have the standard scikit-learn fit/transform or fit/predict
interface.
