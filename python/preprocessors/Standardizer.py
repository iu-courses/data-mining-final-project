import pandas as pd

from sklearn.base import TransformerMixin, BaseEstimator


class Standardizer(BaseEstimator, TransformerMixin):
    """Fit all attributes into the range [0, 1]"""

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        cols = X.columns

        return (X[cols] - X[cols].min()) / (X[cols].max() - X[cols].min())
