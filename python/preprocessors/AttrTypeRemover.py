import numpy as np
from sklearn.base import TransformerMixin, BaseEstimator


class AttrTypeRemover(BaseEstimator, TransformerMixin):
    """Remove certain types of attributes"""

    def __init__(self, remove_ints=False, remove_floats=False, remove_cats=False, remove_bins=False, remove_calcs=False):
        """
        Removing ints will not remove categorical/binary variables.
        However, removing ints/floats/cats/bins will remove calculated attributes of that type.
        """
        self.remove_id = True
        self.remove_ints = remove_ints
        self.remove_floats = remove_floats
        self.remove_cats = remove_cats
        self.remove_bins = remove_bins
        self.remove_calcs = remove_calcs

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        cols_to_keep = X.columns

        # remove ints: keep cats, bins, and floats
        if self.remove_id:
            cols_to_keep = [col for col in cols_to_keep if 'id' not in col]
        if self.remove_ints:
            cols_to_keep = [col for col in cols_to_keep if ('cat' in col or 'bin' in col
                                                         or X.dtypes[col] == np.float64)]
        if self.remove_floats:
            cols_to_keep = [col for col in cols_to_keep if X.dtypes[col] != np.float64]
        if self.remove_cats:
            cols_to_keep = [col for col in cols_to_keep if 'cat' not in col]
        if self.remove_bins:
            cols_to_keep = [col for col in cols_to_keep if 'bin' not in col]
        if self.remove_calcs:
            cols_to_keep = [col for col in cols_to_keep if 'calc' not in col]

        return X[cols_to_keep]
