from sklearn.base import TransformerMixin, BaseEstimator

from python.Data import getVariances


class InvariantRemover(BaseEstimator, TransformerMixin):
    """Removes attributes with low variance"""

    # stores variance for each attribute
    # in transform step, iterate through this to find attributes with sufficient variance

    def __init__(self, threshold=None, attrs_to_drop=None):
        """
        Choose one argument:
        :param threshold: attributes with less variance than this will be removed
        :param attrs_to_drop: the number of attributes to remove from data; least varying attributes deleted first
        """
        self.threshold = threshold
        self.attrs_to_drop = attrs_to_drop

    def fit(self, X, y=None):
        # no conditions specified
        if self.threshold is None and self.attrs_to_drop is None: return self

        variances = getVariances(X)

        if self.threshold is not None:
            # for every key with value > variance threshold, append it to cols_to_keep
            cols_to_keep = []
            for col, var in variances.items():
                if var > self.threshold:
                    cols_to_keep.append(col)
            self.cols_to_keep = cols_to_keep

        elif self.attrs_to_drop is not None:
            # add the key with minimum variance to cols_to_drop; do this attrs_to_drop number of times
            cols_to_drop = []
            for i in range(self.attrs_to_drop):
                col_with_min_variance = min(variances, key=variances.get)
                cols_to_drop.append(col_with_min_variance)
                del variances[col_with_min_variance]

            # and make cols_to_keep every column name that's not also in cols_to_drop
            self.cols_to_keep = [col for col in X.columns if col not in cols_to_drop]

        return self

    def transform(self, X, y=None):
        # no conditions specified
        if self.threshold is None and self.attrs_to_drop is None: return X
        return X[self.cols_to_keep]
