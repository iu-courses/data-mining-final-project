from sklearn.base import TransformerMixin, BaseEstimator


class ResetIndex(BaseEstimator, TransformerMixin):

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        return X.reset_index(drop=True)
