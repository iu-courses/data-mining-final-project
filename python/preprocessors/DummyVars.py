import pandas as pd

from sklearn.base import TransformerMixin, BaseEstimator

from python.Data import getCategoricalColumns


class DummyVars(BaseEstimator, TransformerMixin):
    """Replaces categorical attributes with dummy variables"""

    def fit(self, X, y=None):
        self.catsToDrop = getCategoricalColumns(X)
        return self

    def transform(self, X):
        return pd.get_dummies(X, columns=self.catsToDrop)
