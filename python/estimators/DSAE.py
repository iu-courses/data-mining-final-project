import tensorflow as tf
import sys
import numpy as np

from python.estimators.DBInterface import DBInterface

class DSAE(DBInterface):

    MODEL_PARAMS = ('num_inputs', 'hidden_units', 'batch_size', 'drop_rate', 'optimizer_name', 'learning_rate', 'momentum', 'decay_rate')


    def __init__(self,

                 #graph parameters
                 num_inputs = 0,
                 hidden_units= (),
                 batch_size=128,
                 drop_rate=0.4,
                 optimizer_name='adam',
                 learning_rate=0.2,
                 momentum=0.99,
                 decay_rate=0.98,

                 #training parameters
                 epochs=None,
                 validationX=None,
                 stop_after=10,

                 #loading parameters
                 max_size=3,
                 db_name= 'model-ae.csv',
                 load_config_from_hash=None,
                 use_model_rank=1):



        #database loading
        super(DSAE, self).__init__(db_name, max_size)


        #model parameters
        if load_config_from_hash is None:
            self.num_inputs = num_inputs
            self.hidden_units = hidden_units
            self.batch_size = batch_size
            self.drop_rate = drop_rate
            self.optimizer_name = optimizer_name
            self.learning_rate = learning_rate
            self.momentum = momentum
            self.decay_rate = decay_rate
            setattr(self, self.CONFIG_HASH, self._get_config_hash())
        else:
            if not self._load_config_from_hash(load_config_from_hash):
                raise ValueError("invalid config hash")
            setattr(self, self.CONFIG_HASH, load_config_from_hash)


        #training parameters
        self.epochs = epochs
        self.validationX = validationX
        self.stop_after = stop_after
        self.max_size = max_size
        self.best_so_far = []

        #transformation parameters
        if use_model_rank <= max_size:
            self.use_model_rank = use_model_rank
        else:
            raise ValueError('use_model_rank cannot be greater than max_size')

        #graph reset
        self.graph = self._define_graph()


    ##########################################################
    ##########################################################
    ####### Training
    ##########################################################
    ##########################################################


    def fit(self, X, y=None):
        print("DSAE fit starting")
        VALIDATE_BATCH_SIZE_DIV = 4

        with self.graph.as_default():

            #set up the data sets
            X_init = tf.placeholder(tf.float32, shape=X.shape)
            datasetTrain = tf.data.Dataset.from_tensor_slices(X_init).repeat().shuffle(buffer_size=100000).batch(batch_size=self.batch_size)
            iterTrain = datasetTrain.make_initializable_iterator()
            datasetValidate = tf.data.Dataset.from_tensor_slices(X_init).batch(batch_size=X.shape[0]//VALIDATE_BATCH_SIZE_DIV).repeat()
            iterValidate = datasetValidate.make_initializable_iterator()

            with tf.Session() as sess:

                #run the initializer
                init = tf.global_variables_initializer()
                sess.run([iterTrain.initializer, iterValidate.initializer, init], feed_dict={X_init: X})

                # get some ops from the graph
                next_batch = iterTrain.get_next()
                next_batch_validate = iterValidate.get_next()

                extra_update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)


                ####################
                #begin the training
                ####################
                epochs_since_last_update = 0
                for epoch in range(self.epochs if self.epochs else sys.maxsize):
                    for i in range(X.shape[0] // self.batch_size):
                        X_batch = sess.run(next_batch)
                        sess.run([self.optimizer, extra_update_ops],
                                 feed_dict={self.X: X_batch,
                                            self.training: True})

                    #calculate the validation loss
                    loss = 0
                    for i in range(VALIDATE_BATCH_SIZE_DIV):
                        X_batch = sess.run(next_batch_validate)
                        loss += sess.run(self.loss_score,
                                 feed_dict={self.X: X_batch,
                                            self.training: False})
                    loss /= VALIDATE_BATCH_SIZE_DIV

                    print(epoch, loss)

                    #update the database
                    new_model_id = self._update_db(loss)
                    if new_model_id is not None:
                        epochs_since_last_update = 0
                        self._save_checkpoint(self.saver, sess, new_model_id)
                    else:
                        epochs_since_last_update += 1

                    #early stopping
                    if self.stop_after != None and self.stop_after == epochs_since_last_update:
                        print("Stopping early on epoch", epoch)
                        break


        print("DSAE fit complete")



    ##########################################################
    ##########################################################
    ####### Transformation
    ##########################################################
    ##########################################################

    def transform(self, X):
        bestID = self._get_best_id(self.use_model_rank)



        with tf.Session(graph=self.graph) as sess:
            self._load_checkpoint(self.saver, sess, bestID)

            #get an iterator (for large batches)
            BATCH_SIZE_DIV = 4
            X_init = tf.placeholder(tf.float32, shape=X.shape)
            datasetValidate = tf.data.Dataset.from_tensor_slices(X_init).batch(
                batch_size=X.shape[0] // BATCH_SIZE_DIV)
            iter = datasetValidate.make_initializable_iterator()
            sess.run([iter.initializer], feed_dict={X_init: X})
            next_batch = iter.get_next()

            ys = []

            try:
                while True:
                    next_x = sess.run(next_batch)
                    ys.append(self.encoded.eval(feed_dict={self.X: next_x, self.training: False}))

            except tf.errors.OutOfRangeError:
                return np.concatenate(ys, axis=0)


    ##########################################################
    ##########################################################
    ####### Graph Definitions
    ##########################################################
    ##########################################################

    def _define_graph(self):

        g = tf.Graph()
        with g.as_default():
            self.X = self._create_placeholders()
            self.training = self._create_variables()
            self.tensors, self.encoded = self._connect(input_placeholder=self.X, training=self.training)

            self.loss_score = self._make_loss_function(self.tensors[-1], self.X)
            self.optimizer = self._make_optimizer(loss_tensor=self.loss_score)
            self.saver = tf.train.Saver()
        return g


    #*****************************************
    #Initialize the placeholders and variables
    #*****************************************
    def _create_placeholders(self):
        return tf.placeholder(tf.float32, (None, self.num_inputs))

    def _create_variables(self):
        return tf.Variable(initial_value=True, trainable=False)


    #*****************************************
    #Create the optimization tools
    #*****************************************
    def _make_optimizer(self, loss_tensor):


        #decay the learning rate if properly configured
        global_step = tf.Variable(0, trainable=False, name="global_step")
        if self.learning_rate is not None and self.decay_rate > 0:
            self.learning_rate = tf.train.exponential_decay(learning_rate=self.learning_rate,
                                                       global_step=global_step,
                                                       decay_steps=10000,
                                                       decay_rate=self.decay_rate)

        #choose our optimizer
        if self.optimizer_name == 'nesterov':
            optimizer = tf.train.MomentumOptimizer(learning_rate=self.learning_rate, momentum=self.momentum, use_nesterov=True)
        elif self.optimizer_name == 'momentum':
            optimizer = tf.train.MomentumOptimizer(learning_rate=self.learning_rate, momentum=self.momentum)
        elif self.optimizer_name == 'adam':
            optimizer = tf.train.AdamOptimizer(epsilon=0.01)
        elif self.optimizer_name == 'ada':
            optimizer = tf.train.AdagradOptimizer(learning_rate=0.1)
        else:
            raise Exception("Not a valid optimizer!")

        #bind it to the loss tensor
        return optimizer.minimize(loss_tensor, global_step=global_step)


    def _make_loss_function(self, decoded, inputs):
        with tf.name_scope("loss"):
            return tf.reduce_mean(tf.square(decoded - inputs))

    #*****************************************
    #Create the layers of the graph
    #*****************************************

    def _connect(self, input_placeholder, training):

        #Initializers
        initializers = {
            'xavier': tf.initializers.variance_scaling(mode='fan_avg'),
            'he': tf.initializers.variance_scaling(mode='fan_in')
        }

        #!!!!!!!!!!!!!!!!!!!
        #Conntection Logic
        #!!!!!!!!!!!!!!!!!!

        next = input_placeholder
        tensors = []

        #input dropper
        if self.drop_rate > 0:
            next = tf.layers.dropout(next,
                                    self.drop_rate,
                                    training=training)
            tensors.append(next)

        #set the weight tensors
        #NOTE: This builds the list of tensors from the outside in to do proper weight tieing
        weights = []
        initializer = initializers['he']
        for units, prev_size in zip(self.hidden_units[::-1], self.hidden_units[::-1][1:] + [self.num_inputs]):
            encoding = tf.Variable(initializer([prev_size, units]), dtype=tf.float32)
            decoding = tf.transpose(encoding)
            weights.insert(0,encoding)
            weights.append(decoding)

        #set the bias tensors
        biases = []
        for units in self.hidden_units:
            biases.append(tf.Variable(tf.zeros(units)))
        for units in self.hidden_units[::-1][1:]:
            biases.append(tf.Variable(tf.zeros(units)))
        biases.append(tf.Variable(tf.zeros(self.num_inputs)))


        #connect the tensors
        activation = tf.nn.elu
        for weight, bias in zip(weights[:-1], biases[:-1]):
            next = activation(tf.matmul(next, weight) + bias)
            tensors.append(next)
        next = tf.matmul(next,weights[-1]) + biases[-1]
        tensors.append(next)
        return tensors, tensors[len(tensors) // 2] if self.drop_rate > 0 else tensors[(len(tensors) // 2) - 1]