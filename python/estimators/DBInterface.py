import ast
import hashlib
import pandas as pd
import uuid
import os
import tensorflow as tf
from os import path
import numpy as np


class DBInterface():

    MODEL_PARAMS = ()
    SCORE = 'loss'
    ID = 'uuid'
    CONFIG_HASH = 'config_hash'
    MODEL_DIR = path.abspath(path.join(path.realpath(__file__), os.pardir, os.pardir, os.pardir, "models"))


    def __init__(self, dbname, max_size=3):
        self.db_path = path.join(self.MODEL_DIR, dbname)
        self.max_size = max_size
        self.saver = tf.train.Saver(allow_empty=True)
        self.db = self._load_db()
        self._save_db()

    ##########################################################
    ##########################################################
    ####### Database Utilities
    ##########################################################
    ##########################################################


    #*****************************************
    #Loading the Database
    #*****************************************


    def _load_db(self):
        if not os.path.exists(self.db_path):
            return pd.DataFrame(columns=pd.Index((self.CONFIG_HASH,) + self.MODEL_PARAMS + (self.SCORE,) + (self.ID,)))
        else:
            return pd.read_csv(self.db_path, header=0)


    def _load_config_from_hash(self, hash):

        #load the params
        params = self.db.loc[self.db[self.CONFIG_HASH] == hash, self.MODEL_PARAMS]
        if params.shape[0] == 0:
            return False

        #set the params
        for param in params.columns:
            value = params.loc[:,param].iloc[0]

            if isinstance(value, str) and '[' in value:
                setattr(self, param, ast.literal_eval(value))
            else:
                setattr(self, param, value)

        return True



    #*****************************************
    #Updating the Database
    #*****************************************

    def _save_db(self, backup=False):
        if backup:
            self.db.to_csv(self.db_path + "-backup", index=False)
        else:
            self.db.to_csv(self.db_path, index=False)


    def _update_db(self, last_score):
        prev_scores = self.db.loc[self.db[self.CONFIG_HASH] == getattr(self, self.CONFIG_HASH), self.SCORE].astype(np.float64)
        id = uuid.uuid4().hex

        #early exit if we have less than max size models
        if prev_scores.shape[0] < self.max_size:
            self._write_row(last_score, id)
            self._save_db()
            print("Added model", prev_scores.shape[0] + 1)
            return id


        #find the model to replace
        worse = prev_scores.loc[prev_scores > last_score]
        if worse.shape[0] > 0:
            replacement_index = worse.idxmax()

            #delete the old model
            oldID = self.db.loc[replacement_index, self.ID]
            dirListing = os.listdir(self.MODEL_DIR)
            for item in dirListing:
                if str(oldID) in item:
                    os.remove(path.join(self.MODEL_DIR, item))

            #update the values
            id = uuid.uuid4().hex
            self.db.loc[replacement_index, self.SCORE] = last_score
            self.db.loc[replacement_index, self.ID] = id


            #persist the values
            self._save_db()
            print("Found new model at rank", str(self.max_size - len(worse) + 1))
            return id

        else:
            return None


    def _write_row(self, loss=0, id=None):
        # load it up
        to_add = {param: getattr(self, param) for param in self.MODEL_PARAMS if hasattr(self, param)}
        to_add[self.SCORE] = loss
        to_add[self.ID] = id
        to_add[self.CONFIG_HASH] = getattr(self, self.CONFIG_HASH)

        # append the data
        self.db = self.db.append(to_add, ignore_index=True)

    # *****************************************
    # Etc.
    # *****************************************

    def _get_config_hash(self):
        byte_string = b""

        for param in self.MODEL_PARAMS:
            if hasattr(self, param):
                byte_string += str(getattr(self, param)).encode()

        return hashlib.sha1(byte_string).hexdigest()



    ##########################################################
    ##########################################################
    ####### Checkpoints and Model Saving
    ##########################################################
    ##########################################################


    def _save_checkpoint(self,saver, sess, id):
        saver.save(sess, path.join(self.MODEL_DIR, id), write_meta_graph=False)

    def _load_checkpoint(self, saver, sess, id):
        saver.restore(sess, path.join(self.MODEL_DIR, str(id)))

    def _get_best_id(self, rank):
        scores = self.db.loc[self.db[self.CONFIG_HASH] == getattr(self, self.CONFIG_HASH), self.SCORE].astype(np.float64).sort_values()
        return self.db.loc[scores.index[rank-1], self.ID]