import tensorflow as tf
import sys
import os
from os import path
import numpy as np

from python.estimators.DBInterface import DBInterface
from python.gini import gini_normalized

MODEL_DIR = path.abspath(path.join(path.realpath(__file__), os.pardir, os.pardir, os.pardir, "models"))

class DNN(DBInterface):

    MODEL_PARAMS = (
    'num_classes', 'num_inputs', 'hidden_units', 'batch_size', 'input_drop_rate', 'hidden_drop_rate', 'batch_normalization',
    'optimizer_name', 'learning_rate', 'momentum','decay_rate', 'regulizer','tag')

    def __init__(self,

                 #graph parameters
                 num_classes=0,
                 num_inputs=0,
                 hidden_units=(),
                 batch_size=128,
                 input_drop_rate=0.1,
                 hidden_drop_rate=0.4,
                 batch_normalization=False,
                 optimizer_name='adam',
                 learning_rate=0.2,
                 momentum=0.99,
                 decay_rate=0,
                 regulizer='max_norm',
                 tag=None,

                 #training parameters
                 validationX=None,
                 validationY=None,
                 epochs=None,
                 stop_after=10,

                 #db parameters
                 max_size=3,
                 db_name='model-dnn.csv',
                 load_config_from_hash=None,
                 use_model_rank=1):




        # database loading
        super(DNN, self).__init__(db_name, max_size)

        # model parameters
        if load_config_from_hash is None:
            self.num_inputs = num_inputs
            self.num_classes = num_classes
            self.hidden_units = hidden_units
            self.batch_size = batch_size
            self.input_drop_rate = input_drop_rate
            self.hidden_drop_rate = hidden_drop_rate
            self.batch_normalization = batch_normalization
            self.optimizer_name = optimizer_name
            self.learning_rate = learning_rate
            self.momentum = momentum
            self.decay_rate = decay_rate
            self.regulizer = regulizer
            self.tag = tag
            setattr(self, self.CONFIG_HASH, self._get_config_hash())
        else:
            if not self._load_config_from_hash(load_config_from_hash):
                raise ValueError("invalid config hash")
            setattr(self, self.CONFIG_HASH, load_config_from_hash)

        # training parameters
        self.epochs = epochs
        if((validationX is not None and validationY is None) or (validationY is not None and validationX is None)):
            raise ValueError("Need both validationX and validationY")
        self.validationX = validationX
        self.validationY = validationY
        self.stop_after = stop_after

        # transformation parameters
        if use_model_rank <= max_size:
            self.use_model_rank = use_model_rank
        else:
            raise ValueError('use_model_rank cannot be greater than max_size')

        # graph reset
        self.graph = self._define_graph()


    ##########################################################
    ##########################################################
    ####### Training
    ##########################################################
    ##########################################################


    def fit(self, X, y=None):
        with self.graph.as_default():

            #TODO see if I can implement up sampling of the claim class
            X_init = tf.placeholder(tf.float32, shape=X.shape)
            y_init = tf.placeholder(tf.int32, shape=y.shape)
            datasetTrain = tf.data.Dataset.from_tensor_slices((X_init, y_init)).repeat().shuffle(buffer_size=100000).batch(batch_size=self.batch_size)
            iterTrain = datasetTrain.make_initializable_iterator()


            with tf.Session() as sess:

                # run the initializer
                init = tf.global_variables_initializer()
                sess.run([iterTrain.initializer, init], feed_dict={X_init: X, y_init:y})

                # get some ops from the graph
                next_batch = iterTrain.get_next()
                #next_batch_validate = iterValidate.get_next()

                extra_update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
                ####################
                #begin the training
                ####################
                print("dnn", getattr(self, self.CONFIG_HASH))
                epochs_since_last_update = 0
                print("model", self._get_best_id(self.use_model_rank))
                self._load_checkpoint(self.saver, sess, self._get_best_id(self.use_model_rank))
                for epoch in range(self.epochs if self.epochs else sys.maxsize):

                    # #the epoch loop - goes over all of the training data (well it randomly selects n elements)
                    # for i in range(X.shape[0] // self.batch_size):
                    #
                    #     X_batch, y_batch = sess.run(next_batch)
                    #     sess.run([self.optimizer, extra_update_ops],
                    #              feed_dict={self.X: X_batch,
                    #                         self.y: y_batch,
                    #                         self.training: True})
                    #
                    #     if self.regulizer == 'max_norm':
                    #         sess.run(tf.get_collection("max_norm"))
                    #

                    #at the end of each epoch, let's see how we are doing
                    predicted = sess.run(tf.nn.softmax(self.logits),
                                         feed_dict={self.X: self.validationX,
                                                    self.training: False})[:,1]

                    score = gini_normalized(self.validationY,predicted)
                    print(epoch, score)

                    new_model_id = self._update_db(-score)
                    if new_model_id is not None:
                        epochs_since_last_update = 0
                        self._save_checkpoint(self.saver, sess, new_model_id)
                    else:
                        epochs_since_last_update += 1

                    # early stopping
                    if self.stop_after is not None and self.stop_after == epochs_since_last_update:
                        print("Stopping early on epoch", epoch)
                        break


    ##########################################################
    ##########################################################
    ####### Prediction
    ##########################################################
    ##########################################################

    def predict(self, X):
        bestID = self._get_best_id(self.use_model_rank)

        with tf.Session(graph=self.graph) as sess:
            self._load_checkpoint(self.saver, sess, bestID)

            #get an iterator (for large batches)
            BATCH_SIZE_DIV = 8
            X_init = tf.placeholder(tf.float32, shape=X.shape)
            datasetValidate = tf.data.Dataset.from_tensor_slices(X_init).batch(
                batch_size=X.shape[0] // BATCH_SIZE_DIV)
            iter = datasetValidate.make_initializable_iterator()
            sess.run([iter.initializer], feed_dict={X_init: X})
            next_batch = iter.get_next()

            ys = []

            try:
                while True:
                    next_x = sess.run(next_batch)
                    predicted = sess.run(tf.nn.softmax(self.logits),
                                         feed_dict={self.X: next_x,
                                                    self.training: False})[:, 1]
                    ys.append(predicted)

            except tf.errors.OutOfRangeError:
                return np.concatenate(ys, axis=0)


    ##########################################################
    ##########################################################
    ####### Graph Definitions
    ##########################################################
    ##########################################################

    def _define_graph(self):

        g = tf.Graph()
        with g.as_default():
            self.X, self.y = self._create_placeholders()
            self.training = self._create_variables()
            self.logits = self._connect()
            self.loss_score = self._make_loss_function()
            self.optimizer = self._make_optimizer()
            self.saver = tf.train.Saver()
        return g


    #*****************************************
    #Initialize the placeholders and variables
    #*****************************************
    def _create_placeholders(self):
        return tf.placeholder(tf.float64, (None, self.num_inputs)), tf.placeholder(tf.int64, (None))

    def _create_variables(self):
        return tf.Variable(initial_value=True, trainable=False)


    #*****************************************
    #Create the optimization tools
    #*****************************************
    def _make_optimizer(self):
        #decay the learning rate if properly configured
        global_step = tf.Variable(0, trainable=False, name="global_step")
        if self.learning_rate is not None and self.decay_rate > 0:
            self.learning_rate = tf.train.exponential_decay(learning_rate=self.learning_rate,
                                                       global_step=global_step,
                                                       decay_steps=50000,
                                                       decay_rate=self.decay_rate)

        #choose our optimizer
        if self.optimizer_name == 'nesterov':
            optimizer = tf.train.MomentumOptimizer(learning_rate=self.learning_rate, momentum=self.momentum, use_nesterov=True)
        elif self.optimizer_name == 'momentum':
            optimizer = tf.train.MomentumOptimizer(learning_rate=self.learning_rate, momentum=self.momentum)
        elif self.optimizer_name == 'adam':
            optimizer = tf.train.AdamOptimizer(epsilon=0.01)
        elif self.optimizer_name == 'ada':
            optimizer = tf.train.AdagradOptimizer(learning_rate=0.1)
        else:
            raise Exception("Not a valid optimizer!")

        #bind it to the loss tensor
        return optimizer.minimize(self.loss_score, global_step=global_step)


    def _make_loss_function(self):
        with tf.name_scope("loss"):
            weights = tf.add(tf.cast(self.y, tf.float64), .1)
            return tf.losses.sparse_softmax_cross_entropy(labels=self.y,
                                                          logits=self.logits,
                                                          weights=weights)

    def _make_max_norm_regulizer(self, threshold, axes=1, name="max_norm", collection="max_norm"):

        def max_norm(weights):
            clipped = tf.clip_by_norm(weights, clip_norm=threshold, axes=axes)
            clipped_weights = tf.assign(weights, clipped, name=name)
            tf.add_to_collection(collection, clipped_weights)
            return None
        return max_norm

    #*****************************************
    #Create the layers of the graph
    #*****************************************

    def _connect(self):

        #Initializers
        initializers = {
            'xavier': tf.initializers.variance_scaling(mode='fan_avg'),
            'he': tf.initializers.variance_scaling(mode='fan_in')
        }

        #!!!!!!!!!!!!!!!!!!!
        #Conntection Logic
        #!!!!!!!!!!!!!!!!!!

        next = self.X
        tensors = []

        #input dropper
        if self.input_drop_rate > 0:
            next = tf.layers.dropout(next,
                                    self.input_drop_rate,
                                    training=self.training)
            tensors.append(next)

        if self.regulizer == 'max_norm':
            regulizer = self._make_max_norm_regulizer(1)
        else:
            regulizer = None

        #hidden
        for units in self.hidden_units:
            next = tf.layers.dense(next,
                                   units=units,
                                   # use faster relu if we are going to norm anyways
                                   activation=tf.nn.relu if self.batch_normalization else tf.nn.elu,
                                   kernel_initializer=initializers['xavier'],
                                   kernel_regularizer=regulizer)
            tensors.append(next)

            #apply batch normalization first (if applicable)
            if self.batch_normalization == True:
                next = tf.layers.batch_normalization(inputs=next, training=self.training, momentum=0.999)
                tensors.append(next)

            #then apply dropout (if applicable -- maybe not a good idea to mix with batch normalization)
            if self.hidden_drop_rate > 0:
                next = tf.layers.dropout(next,
                                         self.input_drop_rate,
                                         training=self.training)
                tensors.append(next)

        #NOTE // the tensors list is not stored anywhere but I left the accumulator here in case we need it in the future

        #Attach the output layer
        return tf.layers.dense(next, units=self.num_classes)
