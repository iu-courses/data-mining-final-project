from sklearn.base import TransformerMixin, BaseEstimator
from sklearn.ensemble import RandomForestClassifier
import numpy as np
import multiprocessing


class RandomForest(BaseEstimator, TransformerMixin):

    def __init__(self, numberTrees=500):
        self.numberTrees = numberTrees

    def fit(self, X, y=None):
        self.rf = RandomForestClassifier(n_jobs=multiprocessing.cpu_count(),
                                         n_estimators=self.numberTrees,
                                         max_features=None)
        print("RF fit starting")
        self.rf.fit(X, y)
        print("RF fit done")
        return self

    def predict(self, X, y=None):
        print("RF prediction starting")
        self.predicted = -self.rf.predict_proba(X)
        print("RF prediction done")
        return self.predicted

    def transform(self, X, y=None):
        return self.predict(X)
