import tensorflow as tf
import numpy as np
from sklearn.base import TransformerMixin, BaseEstimator

def create_model(features, params):
    net = tf.feature_column.input_layer({'':features}, params['feature_columns'])
    input = net
    for units in params['hidden_units']:
        net = tf.layers.dense(net, units=units, activation=tf.nn.relu)

    logits = tf.layers.dense(net, params['n_classes'], activation=None, name="output")
    return logits, input


def model_fn(features,labels,mode,params):

    logits, input = create_model(features, params)
    print("Model created")

    predicted_classes = tf.argmax(logits, 1)
    if mode == tf.estimator.ModeKeys.PREDICT:


        predictions = {
            'class_ids': predicted_classes[:, tf.newaxis],
            'probabilities': tf.nn.softmax(logits),
            'logits': logits,
        }
        return tf.estimator.EstimatorSpec(mode, predictions=predictions)

    # Compute loss.
    xe = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=labels, logits=logits)
    weights = tf.add(tf.cast(labels, tf.float32),.1)
    loss = tf.reduce_mean(tf.tensordot(xe, weights, axes=0))


    # Compute evaluation metrics.
    accuracy = tf.metrics.accuracy(labels=labels,
                                   predictions=predicted_classes,
                                   name='acc_op')

    tf.summary.scalar('accuracy', accuracy[1])

    if mode == tf.estimator.ModeKeys.EVAL:
        metrics = {'accuracy': accuracy}
        predictions = {
            'class_ids': predicted_classes[:, tf.newaxis],
            'probabilities': tf.nn.softmax(logits),
            'logits': logits,
        }
        return tf.estimator.EstimatorSpec(
            mode, loss=loss, predictions=predictions, eval_metric_ops=metrics)

    optimizer = tf.train.AdagradOptimizer(learning_rate=0.1)
    train_op = optimizer.minimize(loss, global_step=tf.train.get_global_step())
    return tf.estimator.EstimatorSpec(mode, loss=loss, train_op=train_op)


class MyHook(tf.train.SessionRunHook):

    def __init__(self, input):
        super()
        self.input = input

    def after_create_session(self, session, coord):
        self.sessionNum = 0

    def before_run(self, run_context):
        print(run_context.original_args)

    def after_run(self,
                run_context,
                run_values):
        print(run_context.original_args)
        print(run_values.results)


def DNNEstimator(X):

    return tf.estimator.Estimator(
        model_fn=model_fn,
        model_dir= '/tmp/tftest',
        params= {
            'feature_columns': tf.contrib.learn.infer_real_valued_columns_from_input(X),
            'hidden_units': [500,500],
            'n_classes': 2,

        }
    )


def train_input_fn(features, labels, batch_size):
    print("Making dataset")
    """An input function for training"""
    # Convert the inputs to a Dataset.
    dataset = tf.data.Dataset.from_tensor_slices((features, labels))
    print("Made dataset")
    # Shuffle, repeat, and batch the examples.
    dataset = dataset.shuffle(10000).repeat().batch(batch_size)

    # Return the read end of the pipeline.
    return dataset.make_one_shot_iterator().get_next()


def test_input_fn(features):
    print("Making dataset")
    """An input function for testing"""
    # Convert the inputs to a Dataset.
    dataset = tf.data.Dataset.from_tensor_slices((features))

    print("Made dataset")
    # Shuffle, repeat, and batch the examples.
    dataset = dataset.batch(features.shape[0])

    # Return the read end of the pipeline.
    return dataset.make_one_shot_iterator().get_next()

def validate_input_fn(features, labels):
    print("Making dataset")
    """An input function for testing"""
    # Convert the inputs to a Dataset.
    dataset = tf.data.Dataset.from_tensor_slices((features, labels))

    print("Made dataset")
    # Shuffle, repeat, and batch the examples.
    dataset = dataset.batch(features.shape[0])

    # Return the read end of the pipeline.
    return dataset.make_one_shot_iterator().get_next()


class MyExporter(tf.estimator.Exporter):

    name = 'hey'

    def export(self, estimator, export_path, checkpoint_path, eval_result,
             is_the_final_export):
        print(eval_result)


class EstimatorTest(BaseEstimator, TransformerMixin):

    def __init__(self, ValidationX, ValidationY):
        self.ValidationX = ValidationX
        self.ValidationY = ValidationY


    def fit(self, X, y=None):
        tf.logging.set_verbosity(tf.logging.INFO)
        print("Custom Estimator fit starting")
        self.dnn = DNNEstimator(X)
        #
        # train_spec = tf.estimator.TrainSpec(input_fn=lambda:train_input_fn(X,y,128), max_steps=10000)
        # eval_spec = tf.estimator.EvalSpec(input_fn=lambda:validate_input_fn(self.ValidationX, self.ValidationY),
        #                                   steps=1)
        # tf.estimator.train_and_evaluate(self.dnn, train_spec, eval_spec)

        self.dnn.train(
            input_fn=lambda:train_input_fn(X,y,128),
            steps=100000
        )
        return self

    def predict(self, X):
        print("TF prediction starting")
        return self.dnn.predict(
            input_fn=tf.estimator.inputs.numpy_input_fn(x=np.array(X), num_epochs=1, batch_size=X.shape[0], shuffle=False),
            yield_single_examples=False
        )


