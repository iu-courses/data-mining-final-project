from functools import partial

import numpy as np
from sklearn.base import TransformerMixin, BaseEstimator
import tensorflow as tf


class DNAE(BaseEstimator, TransformerMixin):

    def __init__(self, noise_method, noise_rate, hidden_layer_sizes, n_iterations):
        self.noise_method = noise_method
        self.noise_rate = noise_rate
        self.hidden_layer_sizes = hidden_layer_sizes
        self.n_iterations = n_iterations

    def fit(self, X, y=None):
        print("Stacked autoencoder fit starting")
        self.learning_rate = 0.01

        self.n_inputs = X.shape[1]
        self.n_outputs = X.shape[1]
        # self.l_input = tf.placeholder(tf.float32, shape=[None, self.n_inputs])
        # self.l_hidden = tf.layers.dense(self.l_input, self.n_hidden)
        # self.l_output = tf.layers.dense(self.l_hidden, self.n_outputs)
        #
        # self.reconstruction_loss = tf.reduce_mean(tf.square(self.l_output - self.l_input))
        # self.optimizer = tf.train.AdamOptimizer(self.learning_rate)
        # self.training_op = self.optimizer.minimize(self.reconstruction_loss)
        # self.init = tf.global_variables_initializer()
        #
        # self.l_codings = self.l_hidden
        # self.saver = tf.train.Saver()
        #
        # with tf.Session() as sess:
        #     self.init.run()
        #     for iteration in range(self.n_iterations):
        #         self.training_op.run(feed_dict={self.l_input: X})
        #         if iteration % 20 == 0: print("Iteration {} complete".format(iteration))
        #
        #     # save the TF session so it can be used in transform
        #     save_path = self.saver.save(sess, "/tmp/TFFeatureRemoval.ckpt")

        my_dense_layer = partial(tf.layers.dense,
                                 activation=tf.nn.elu,
                                 kernel_initializer=tf.contrib.layers.variance_scaling_initializer(),
                                 kernel_regularizer=tf.contrib.layers.l2_regularizer(0.0001))

        self.l_input = tf.placeholder(tf.float32, shape=[None, self.n_inputs])

        if self.noise_method == 'gaussian':
            self.l_input_noisy = self.l_input + self.noise_rate * tf.random_normal(tf.shape(self.l_input))
        elif self.noise_method == 'dropout':
            self.l_training = tf.placeholder_with_default(False, shape=())
            self.l_input_noisy = tf.layers.dropout(self.l_input, self.noise_rate, training=self.l_training)
        else:
            self.l_input_noisy = self.l_input
        # l_hidden1 = my_dense_layer(self.l_input_noisy, self.n_hidden1, activation=tf.nn.relu)
        # l_hidden2 = my_dense_layer(l_hidden1, self.n_hidden2)
        # l_hidden3 = my_dense_layer(l_hidden2, self.n_hidden3)
        # self.l_output = my_dense_layer(l_hidden3, self.n_outputs, activation=None)
        if not self.hidden_layer_sizes:
            self.l_output = my_dense_layer(self.l_input_noisy, self.n_outputs, activation=None)
        else:
            # First, make the first hidden layer
            self.l_hidden_1 = my_dense_layer(self.l_input_noisy, self.hidden_layer_sizes[0], activation=tf.nn.relu)
            # Then, make the rest
            for i in range(1, len(self.hidden_layer_sizes)):
                # But if it's the coding layer, set it to l_coding
                if i == len(self.hidden_layer_sizes) // 2:
                    self.l_coding = my_dense_layer(getattr(self, "l_hidden_{}".format(i)),
                                                   self.hidden_layer_sizes[i])
                    # also set the l_hidden_n attr so next layer can link to this
                    setattr(self, "l_hidden_{}".format(i + 1), self.l_coding)
                # Otherwise, set it to l_hidden_n
                else:
                    setattr(self, "l_hidden_{}".format(i + 1),
                            my_dense_layer(getattr(self, "l_hidden_{}".format(i)),
                                           self.hidden_layer_sizes[i]))
            # And finally, link the last hidden layer to the output layer
            self.l_output = my_dense_layer(getattr(self,
                                                   "l_hidden_{}".format(len(self.hidden_layer_sizes))),
                                           self.n_outputs,
                                           activation=None)

        reconstruction_loss = tf.reduce_mean(tf.square(self.l_output - self.l_input))
        reg_losses = tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES)
        loss = tf.add_n([reconstruction_loss] + reg_losses)
        optimizer = tf.train.AdamOptimizer(0.01)
        training_op = optimizer.minimize(loss)

        init = tf.global_variables_initializer()

        self.saver = tf.train.Saver()
        with tf.Session() as sess:
            init.run()
            for iteration in range(self.n_iterations):
                if self.noise_method == 'dropout':
                    training_op.run(feed_dict={self.l_input: X, self.l_training: True})
                else:
                    training_op.run(feed_dict={self.l_input: X})
            self.saver.save(sess, "/tmp/StackedAE.ckpt")

        print("Stacked autoencoder fit done")
        return self

    def transform(self, X, y=None):
        print("Stacked autoencoder transform starting")

        # with tf.Session() as sess:
        #     print(sess.run(tf.contrib.memory_stats.BytesInUse()))
        #     # load saved TF session
        #     self.saver.restore(sess, "/tmp/TFFeatureRemoval.ckpt")
        #
        #     self.codings = self.l_codings.eval(feed_dict={self.l_input: np.array(X)[0:100, :]})
        #     print(sess.run(tf.contrib.memory_stats.BytesInUse()))
        with tf.Session() as sess:
            self.saver.restore(sess, "/tmp/StackedAE.ckpt")
            self.output = self.l_coding.eval(feed_dict={self.l_input: X})

        print("Stacked autoencoder transform done")
        # print(self.codings)
        return self.output
