import tensorflow as tf
import numpy as np
import scipy as sp
from python.gini import gini_normalized


class OldGenerator():


    def __init__(self, hidden_topology, output_size, batch_size, X_validation, y_validation):

        self.hidden_topology = hidden_topology
        self.output_size = output_size
        self.batch_size = batch_size
        self.X_validtion = X_validation
        self.y_validation = y_validation
        self.savePath = None


    def fit(self, X, y=None):
        self.graph = tf.Graph()
        with self.graph.as_default(), tf.name_scope("dnn"):
            self._define_graph(X.shape[1])
            print(self.graph.get_operations())
            saver = tf.train.Saver()
            dataset = tf.data.Dataset.from_tensor_slices((X, y)).repeat().shuffle(buffer_size=100000).batch(batch_size=self.batch_size)
            iter = dataset.make_one_shot_iterator()


            with tf.Session() as sess:
                init = tf.global_variables_initializer()
                init.run()
                #sess.run(self.iter.initializer, feed_dict={self.X: X.as_matrix(), self.y: y.as_matrix()})
                #print(self.data)
                next_batch = iter.get_next()
                for epoch in range(1000):
                    for i in range(1000):
                        X_batch, y_batch = sess.run(next_batch)
                        sess.run(self.optimizer, feed_dict={self.X: X_batch, self.y: y_batch})

                    predicted = sess.run(tf.nn.softmax(self.finalTensor), feed_dict={self.X: self.X_validtion})[:,1]
                    print(epoch, gini_normalized(self.y_validation,predicted))
                self.savePath = saver.save(sess, "./my_model.ckpt")


    def predict(self, X):

        with tf.Session(graph=self.graph) as sess:
            saver = tf.train.Saver()
            saver.restore(sess, self.savePath)
            predictions = self.finalTensor.eval(feed_dict={self.X: X})[:,1]
            print(predictions)
            return predictions


    def _define_graph(self, num_inputs):

        self.X, self.y = self._create_placeholders(num_inputs)

        self.hidden_layers = self._make_hidden_layers()
        self.output = self._make_output_layer()

        path = [self.X]
        path += self.hidden_layers
        path.append(self.output)
        self.finalTensor = self._connect(path)

        self.loss = self._make_loss_function(self.finalTensor, self.y)
        self.optimizer = self._make_optimizer(self.loss)


    def _create_placeholders(self, num_inputs):
        return tf.placeholder(tf.float64, (None, num_inputs)), tf.placeholder(tf.int32, (None))


    def _make_optimizer(self, loss):
        print("Making optimizer")
        optimizer = tf.train.AdagradOptimizer(learning_rate=0.1)
        return optimizer.minimize(loss, global_step=tf.train.get_global_step())


    def _make_loss_function(self, output, labels):
        print("Making loss function")
        with tf.name_scope("loss"):
            weights = tf.add(tf.cast(labels, tf.float64), .1)
            return tf.losses.sparse_softmax_cross_entropy(labels=labels,
                                                          logits=output,
                                                          weights=weights)


    def _make_hidden_layers(self):
        print("Creating hidden layers: ", self.hidden_topology)
        hidden_layers = []

        for i,size in enumerate(self.hidden_topology):
            hidden_layers.append(tf.layers.Dense(units = size,
                                                 activation=tf.nn.relu,
                                                 name = "hidden-" + str(i) + "-" + str(size)))
        return hidden_layers

    def _make_output_layer(self):
        print("Making output layer:", self.output_size)
        return tf.layers.Dense(units = self.output_size,
                                name = "output")

    def _connect(self, layers):
        #Conntection Logic
        next = self.X
        tensors = []
        for units in self.hidden_topology:
            next = tf.layers.dense(next,
                                   units=units,
                                   activation=tf.nn.relu,)
                                   #kernel_initializer=initializers['he'])
            tensors.append(next)

            # if batch_normalization == True:
            #     next = tf.layers.batch_normalization(inputs=next, training=training, momentum=0.999)
            #     tensors.append(next)


        #Attach the output layer
        last = tf.layers.dense(next, units=self.output_size)
        tensors.append(last)
        print(tensors)

        return last
