from sklearn.base import TransformerMixin, BaseEstimator
import os
import tensorflow as tf



class TFBase(BaseEstimator, TransformerMixin):

    def __init__(self):
        pass

    def fit(self, X, y=None):
        tf.logging.set_verbosity(tf.logging.INFO)

        print("TF fit starting")
        feature_cols = tf.contrib.learn.infer_real_valued_columns_from_input(X)
        print(feature_cols)
        dnn = tf.contrib.learn.DNNClassifier(hidden_units=[5000, 5000, 5000], n_classes=2, feature_columns=feature_cols)
        self.dnn = tf.contrib.learn.SKCompat(dnn)
        self.dnn.fit(X,y,batch_size=128, steps=10000)
        print("TF fit done")
        return self

    def predict(self, X, y=None):
        print("TF prediction starting")
        self.predicted = self.dnn.predict(X)["probabilities"][:,1]
        print("TF prediction done")
        return self.predicted
