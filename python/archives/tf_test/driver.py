import tensorflow as tf

class Test():

    def fit(self, X1, y1=None):
        n_inputs = X1.shape[1]
        n_hidden1 = 1000
        n_hidden2 = 1000
        n_hidden3 = 1000
        n_outputs = 2

        self.X = tf.placeholder(tf.float32, shape=(None, n_inputs), name="X")
        y = tf.placeholder(tf.int32, shape=(None), name="y")
        self.training = tf.placeholder_with_default(False, shape=(), name='training')
        with tf.name_scope("dnn"):
            hidden1 = tf.layers.dense(self.X, n_hidden1, name="hidden1", kernel_initializer=tf.glorot_uniform_initializer())
            hidden2 = tf.layers.dense(hidden1, n_hidden2, name="hidden2" , kernel_initializer=tf.glorot_uniform_initializer())
            hidden3 = tf.layers.dense(hidden2, n_hidden3, name="hidden3", kernel_initializer=tf.glorot_uniform_initializer())
            self.logits = tf.layers.dense(hidden3, n_outputs, name="outputs", kernel_initializer=tf.glorot_uniform_initializer())


        with tf.name_scope("loss"):
            loss = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=y, logits=self.logits, loss_reduction=tf.losses.Reduction.SUM)
            #loss = tf.reduce_sum(xentropy, name="loss")

        learning_rate = 0.01

        with tf.name_scope("train"):
            optimizer = tf.train.AdagradOptimizer(learning_rate=0.05)
            training_op = optimizer.minimize(loss)

        # with tf.name_scope("eval"):
        #     correct=tf.nn.in_top_k(self.logits, y, 1)
        #     accuracy=tf.reduce_mean(tf.cast(correct, tf.float32))

        init = tf.global_variables_initializer()

        dataset = tf.data.Dataset.from_tensor_slices((X1, y1)).repeat().shuffle(buffer_size=10000).batch(
            batch_size=150)
        iter = dataset.make_one_shot_iterator()

        n_epochs = 100

        extra_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        self.saver = tf.train.Saver()
        with tf.Session() as sess:
            init.run()
            next_batch = iter.get_next()
            for epoch in range(n_epochs):
                for i in range(100):
                    X_batch, y_batch = sess.run(next_batch)
                    sess.run([training_op, extra_ops], feed_dict={self.X: X_batch, y: y_batch, self.training:True})
                #acc_train = accuracy.eval(feed_dict={self.X: X_batch, y: y_batch})
                #acc_val = accuracy.eval(feed_dict={self.X:X1, y:y1})
                print(epoch)

            save_path = self.saver.save(sess, "./my_model_final.ckpt")

    def predict(self,X):
        with tf.Session() as sess:
            self.saver.restore(sess, "./my_model_final.ckpt")
            Z = self.logits.eval(feed_dict={self.X:X})
            print(Z)
            return Z[:,1]