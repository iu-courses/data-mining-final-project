from functools import partial

import numpy as np
from sklearn.base import TransformerMixin, BaseEstimator
import tensorflow as tf


class DNAE(BaseEstimator, TransformerMixin):

    def __init__(self, noise_method, noise_rate, sizes, n_iterations):
        self.noise_method = noise_method
        self.noise_rate = noise_rate
        self.sizes = sizes
        self.n_iterations = n_iterations
        self.learning_rate = 0.01

    def fit(self, X, y=None):
        print("Autoencoder fit starting")

        n_inputs = X.shape[1]
        n_outputs = X.shape[1]

        # all comments here use the below example... 5 hidden layers, 6 total layers excluding output
        # the output layer and coding layers aren't named specially; a specifically named variable
        #     just gets pointed to their actual layer at the end
        # convert          [150, 100, 50, 100, 150]
        # to            [_, 150, 100, 50, 100, 150, 215]
        # layer number: in   1    2    3   4    5    6 = output

        # coding layer number = (number layers / 2)
        # e.g. above 6 total layers (incl. output), coding layer = hidden layer 3

        n_hidden_layers = len(self.sizes)  # 5 in this example
        n_layers = len(self.sizes) + 1  # include output layer, == 6 in this example
        # when iterating over layers, we need to go to n_layers + 1 to compensate for the _ added to the beginning

        sizes = [0] + self.sizes + [n_outputs]
        # for some reason this gets left as a double instead of becoming an int, so use // instead of just /
        n_coding_layer = n_layers // 2

        # add noise to l_input, to get l_input_noisy
        self.l_input = tf.placeholder(tf.float32, shape=[None, n_inputs])
        if self.noise_method == 'gaussian':
            self.l_input_noisy = self.l_input + self.noise_rate * tf.random_normal(tf.shape(self.l_input))
        elif self.noise_method == 'dropout':
            self.l_training = tf.placeholder_with_default(False, shape=())
            self.l_input_noisy = tf.layers.dropout(self.l_input, self.noise_rate, training=self.l_training)
        else:  # no noise
            self.l_input_noisy = self.l_input

        # generate biases
        # for all layers, biases_l = tf.Variable(tf.zeros(n_l))
        for i in range(1, n_layers + 1):  # include final output layer
            setattr(self, "biases_{}".format(i), tf.Variable(tf.zeros(sizes[i])))

        # generate weights
        # for layer 1, weights = tf.Variable(initializer([n_input, n_1])
        # for layers up to and including the coding layer, its [n_(l-1), n_l]
        #     generated for layers in the for loop from 2 to n_coding_layer (inclusive)
        # then, for layers from n_coding_layer + 1 to n_layers (inclusive), it's
        #     weights_i transpose = weights_(n_coding_layer - (i - n_coding_layer) + 1)
        #                         = weights_(2*n_coding_layer - i + 1)
        # e.g. n_coding_layer = 3, weights_1, weights_2, weights_3 generated normally
        # for layer 4, it gets set to layer 2, which is (3 - (4-3) + 1) = 3-1+1
        #   and for layer 5, it's (3 - (5-3) + 1) = 3-2+1 = same as layer 2 transpose
        initializer = tf.contrib.layers.variance_scaling_initializer()
        self.weights_1 = tf.Variable(initializer([n_inputs, sizes[1]]))  # input layer
        for i in range(2, n_coding_layer + 1):  # include the coding layer
            setattr(self, "weights_{}".format(i), tf.Variable(initializer([sizes[i - 1], sizes[i]])))
        for i in range(n_coding_layer + 1, n_layers):  # don't include output layer
            setattr(self, "weights_{}".format(i),
                    tf.transpose(getattr(self, "weights_{}".format(2 * n_coding_layer - i + 1))))
        setattr(self, "weights_{}".format(n_layers), tf.transpose(self.weights_1))  # output layer

        # for i in range(1, n_layers + 1):
        #     print("Layer {}".format(i))
        #     print("Weights dimension = {}".format(getattr(self, "weights_{}".format(i)).shape))

        # generate the layers
        # l_i = tf.nn.elu(tf.matmul(l_(i-1), weights_i) + biases_i)
        self.l_1 = tf.nn.elu(tf.matmul(self.l_input_noisy, self.weights_1) + getattr(self, "biases_1"))
        for i in range(2, n_layers + 1):
            setattr(self, "l_{}".format(i),
                    tf.nn.elu(tf.matmul(getattr(self, "l_{}".format(i - 1)),
                                        getattr(self, "weights_{}".format(i)))
                              + getattr(self, "biases_{}".format(i))))

        # point the coding and output layers to the right things
        self.l_coding = getattr(self, "l_{}".format(n_coding_layer))
        self.l_output = getattr(self, "l_{}".format(n_layers))

        # setup regularizer for loss function
        regularizer = tf.contrib.layers.l2_regularizer(0.0001)
        reg_loss = 0
        # reg_loss = regularizer(weights_1) + regularizer(weights_2) + ...
        for i in range(1, n_layers + 1):
            reg_loss += regularizer(getattr(self, "weights_{}".format(i)))

        # setup optimizer
        reconstruction_loss = tf.reduce_mean(tf.square(self.l_output - self.l_input))
        loss = reconstruction_loss + reg_loss
        optimizer = tf.train.AdamOptimizer(self.learning_rate)
        training_op = optimizer.minimize(loss)

        init = tf.global_variables_initializer()
        self.saver = tf.train.Saver()
        with tf.Session() as sess:
            init.run()
            for iteration in range(self.n_iterations):
                if self.noise_method == 'dropout':
                    training_op.run(feed_dict={self.l_input: X, self.l_training: True})
                else:
                    training_op.run(feed_dict={self.l_input: X})
            self.saver.save(sess, "/tmp/AE.ckpt")

        print("Autoencoder fit done")
        return self

    def transform(self, X, y=None):
        print("Autoencoder transform starting")
        with tf.Session() as sess:
            self.saver.restore(sess, "/tmp/AE.ckpt")
            self.output = self.l_coding.eval(feed_dict={self.l_input: X})
        print("Autoencoder transform done")
        return self.output
