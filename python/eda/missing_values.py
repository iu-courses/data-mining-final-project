import missingno as msno
import matplotlib.pyplot as plt
import numpy as np

from python.Data import Data, getCategoricalColumns, getIntervalColumns
from python.preprocessors.NACreator import NACreator

PLOT_DIR = "plots/"

data = Data()
X, Y, _ = data.getSplit()

missing_value_columns = ["ps_car_14",
                         "ps_reg_03",
                         "ps_ind_05_cat",
                         "ps_car_03_cat",
                         "ps_car_05_cat",
                         "ps_car_07_cat",
                         "ps_car_09_cat"]


def generateMissingValueGraphs():
    na = NACreator()

    #categorical
    msno.matrix(na.fit_transform(X=X[getCategoricalColumns(X)]))
    plt.gcf().subplots_adjust(top=0.75)
    plt.savefig(PLOT_DIR + 'missing_values_cat.png', pad_inches=2)

    # other
    msno.matrix(na.fit_transform(X=X[getIntervalColumns(X)]))
    plt.gcf().subplots_adjust(top=0.75)
    plt.savefig(PLOT_DIR + 'missing_values_int.png', pad_inches=2)


def generateMissingValueHeatmaps(missing_value_columns):
    na = NACreator()
    msno.heatmap(na.fit_transform(X=X[missing_value_columns]))
    plt.savefig(PLOT_DIR + 'missing_values_heatmap.png', pad_inches=2)



def generateMissingValueClassPrevalence(missing_value_columns):
    vals = []
    for column in missing_value_columns:
        missingRows = np.where(X[column] == -1)
        nonMissingRows = np.where(X[column] != -1)

        classFromMissing = Y.iloc[tuple(missingRows)]
        classFromNotMissing = Y.iloc[tuple(nonMissingRows)]

        percentFromMissing = np.sum(classFromMissing) / classFromMissing.size
        percentFromNotMissing = np.sum(classFromNotMissing) / classFromNotMissing.size

        vals.append(percentFromMissing / percentFromNotMissing)

    print(missing_value_columns)
    fig, ax = plt.subplots()
    plt.bar(np.arange(len(missing_value_columns)), vals)
    ax.set_xticks(np.arange(len(missing_value_columns)))
    ax.set_xticklabels(missing_value_columns, rotation=90)
    ax.axhline(1, color='black', linestyle='--')
    plt.gcf().subplots_adjust(bottom=0.25)
    plt.savefig(PLOT_DIR + 'missing_values_class.png')


if __name__ == "__main__":

    #generateMissingValueGraphs()
    #generateMissingValueHeatmaps(missing_value_columns)
    #generateMissingValueClassPrevalence(missing_value_columns)

    pass



