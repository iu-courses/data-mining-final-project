from sklearn.pipeline import Pipeline
from os import path
import pandas as pd
import os

from python.preprocessors.AttrTypeRemover import AttrTypeRemover
from python.preprocessors.NACreator import NACreator
from python.preprocessors.NAImputers import NAImputer_Categorical, NAImputer_Interval
from python.preprocessors.Standardizer import Standardizer
from python.preprocessors.ResetIndex import ResetIndex
from python.preprocessors.DummyVars import DummyVars

from python.estimators.DSAE import DSAE
from python.estimators.DNN import DNN

from python.Data import Data
from python.gini import gini_normalized

pipeline = Pipeline([
    ('ResetIndex', ResetIndex()),
    ('RemoveAttributes', AttrTypeRemover(remove_calcs=True)),
    ('MakeNA', NACreator()),
    ('FixNACategory', NAImputer_Categorical()),
    ('FixNAInterval', NAImputer_Interval(0.05)),
    ('DummyVar', DummyVars()),
    ('Standardize', Standardizer())
], memory=None)

if __name__ == "__main__":
    data = Data()
    pipeline.fit(data.get_all_predictors())

    ################################
    ##Stage 1 - Training Autoencoder
    ################################

    # X = pipeline.transform(data.get_all_predictors())
    # hidden_unit_configs = [
    #     [1500,1500],
    #     # [1500,250],
    #     # [1500,100],
    # ]
    #
    # dropouts = [.1,.2,.3,.4]
    #
    # for hidden_units in hidden_unit_configs:
    #     for dropout in dropouts:
    #         DSAE(num_inputs=X.shape[1],
    #              hidden_units=hidden_units,
    #              drop_rate=dropout,
    #              batch_size=128,
    #              stop_after=5,
    #              epochs=100).fit(X)
    #


    ####################################
    ##Stage 2 - Training DNN
    ####################################

    # X_orig = pipeline.transform(data.trainX)
    # X_valid_orig = pipeline.transform(data.testX)
    #
    # for ae_hash in ["9b3c00707d75d0d6d3454f8eec131a06b8060a0e","68f1213a9f8f3518252aee60a895956b58ec4da5",
    #                 "e31310773eee1b1629aab2e22da896163817123e", "df54613a760724acbd8fd4d38c4396d9565b8432", None]:
    #
    #     if ae_hash is not None:
    #         ae = DSAE(load_config_from_hash="e31310773eee1b1629aab2e22da896163817123e", use_model_rank=2) #TODO change this away from static
    #         X = ae.transform(X_orig)
    #         X_valid = ae.transform(X_valid_orig)
    #     else:
    #         X_valid = X_valid_orig
    #         X = X_orig
    #
    #     for hidden_topology in [[500,250], [500,250,50]]:
    #         for optimizer in ['adam', 'nesterov']:
    #             for lr in [0.001,0.0001, 0.00001] if optimizer == 'nesterov' else [None]:
    #                 for reg in ['batch_norm', 'dropout']:
    #                     print("ae", ae_hash)
    #                     if reg == 'batch_norm':
    #                         dnn = DNN(
    #                             tag=ae_hash,
    #                             num_classes=2,
    #                             num_inputs=X.shape[1],
    #                             hidden_units=hidden_topology,
    #                             optimizer_name=optimizer,
    #                             learning_rate=lr,
    #                             input_drop_rate=0,
    #                             hidden_drop_rate=0,
    #                             batch_normalization=True,
    #                             batch_size=128,
    #                             validationX=X_valid,
    #                             validationY=data.testY).fit(X,data.trainY)
    #                     else:
    #                         for dr in [0.3,0.4,0.5]:
    #                             dnn = DNN(
    #                                 tag=ae_hash,
    #                                 num_classes=2,
    #                                 num_inputs=X.shape[1],
    #                                 hidden_units=hidden_topology,
    #                                 optimizer_name=optimizer,
    #                                 learning_rate=lr,
    #                                 hidden_drop_rate=dr,
    #                                 batch_normalization=False,
    #                                 batch_size=128,
    #                                 validationX=X_valid,
    #                                 validationY=data.testY).fit(X,data.trainY)

    ####################################
    ##Stage 3 - Generating Submission Data
    ####################################

    csv_save = path.abspath(path.join(path.realpath(__file__), os.pardir, os.pardir, os.pardir, "submit/"))
    db = pd.read_csv(path.abspath(path.join(path.realpath(__file__), os.pardir, os.pardir, os.pardir, "models/model-dnn.csv")))
    hashes = db.loc[:,('config_hash','tag')].drop_duplicates()
    X_orig = pipeline.transform(data.process)

    for hash in hashes['config_hash']:#zip(hashes['config_hash'], hashes['tag'])

        #predict
        X = DSAE(load_config_from_hash="e31310773eee1b1629aab2e22da896163817123e", use_model_rank=2).transform(X_orig)
        predicted = DNN(load_config_from_hash=hash).predict(X)

        #save
        pd.DataFrame({
            'id': data.process['id'],
            'target': predicted
        }).to_csv(path.join(csv_save, hash + '.csv'), index=False)


